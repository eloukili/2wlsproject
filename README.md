
# Getting started


## Installation

Clone the repository 

``` git clone https://eloukili@bitbucket.org/eloukili/2wlsproject.git ```

Switch to the repo folder  

``` cd 2wlsProject ```

Install all the dependencies using composer  

```  composer install  ```

Copy the example env file and make the required configuration changes in the .env file 

```cp .env.example .env```

Generate a new application key  

```php artisan key:generate```

Set the database connection in .env  

Run the database migrations  
```php artisan migrate```

Run Countries seeds  
``` php artisan db:seed --class=CountriesTableSeeder ```

Start the local development server  
```php artisan serve```

You can now access the server at http://localhost:8000
