<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Countries;
use App\Inscriptions;

class InscriptionController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hour = \Carbon\Carbon::now()->format('H');
        $etat = false; // true : valide, false: en attente
        if((int)$hour >= 12 && (int)$hour <=21){
            $etat = true;
        }
        else{
            $etat = false; 
        }
        $countries = Countries::all();
        return view('inscription', compact('countries','etat'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        $dateOfBirth = $request->get('bdate');
        $country = $request->get('pays');
        $inscriptions = Inscriptions::all();
        $totalInscriptions = $inscriptions->count();
        $totalAge = 0;
        $age = \Carbon\Carbon::parse($dateOfBirth)->diff(\Carbon\Carbon::now())->format('%y');

        if($country === 'Morocco'){
            if($age < 16){               
                return back()->with('error', ' l’âge minimum doit être de 16 ans.');
            }
        }
        else{
            if($age < 18){               
                return back()->with('error', ' l’âge minimum doit être de 18 ans.');
            }
        }

        if($totalInscriptions >= 3){
            foreach($inscriptions as $inscription){
                $totalAge +=$inscription->age;
            }  
            $ageMoyenne = $totalAge / $totalInscriptions;
            $tennPercentOfAge = (10 * $ageMoyenne) / 100;
            $ageA = $ageMoyenne - $tennPercentOfAge;
            $ageB = $ageMoyenne + $tennPercentOfAge;
            if(!($age >= $ageA && $age <= $ageB)){
                return back()->with('error', ' l’âge ('.$age.') doit être entre la moyenne
                des âges + 10% ('.$ageA.') et la moyenne des âges - 10% ('.$ageB .' ) .');
            }
        }
        $rules = [
            'fname' => 'required',
            'lname' => 'required',
            'bdate' => 'required',
            'sexe' => 'required',
            'phone' => ['required',' regex:/((06)|(07)|(05))[0-9]{8}/'],
            'pays' => 'required',
            'rdate' => 'required',           
        ];
        $request->validate($rules, [
                'fname.required' => 'Prenom is required',
                'lname.required' => 'Nom is required',
                'bdate.required' => 'Date de naissance is required',
                'sexe.required' => 'Sexe is required',
                'fname.required' => 'Name is required',
                'phone.required' => 'Numero de telephone is required',
                'pays.required' => 'pays is required',
                'rdate.required' => 'date d\'inscription is required',
            ]);
   
        $input = $request->all();
        $input['age'] = $age;
        $client = Inscriptions::create($input);
    
        return back()->with('success', 'Client Ajouter avec success.');
    }
  
}
