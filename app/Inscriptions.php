<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscriptions extends Model
{
    protected $fillable = [
        'fname', 'lname', 'bdate','sexe','phone','pays','rdate','etat','age'
        ];
}
