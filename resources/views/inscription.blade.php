<!DOCTYPE html>
<html>
<head>
    <title>Laravel 7 form validation example - ItSolutionStuff.com</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

 
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
  
        <h1>Formulaire d'inscription</h1>
   
        @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
            @php
                Session::forget('error');
            @endphp
        </div>
        @endif
   
        <form method="POST" action="{{ url('client/create') }}">
  
            {{ csrf_field() }}
  
            <div class="form-group">
                <label>Nom:</label>
                <input type="text" name="lname" class="form-control" placeholder="Nom">
                @if ($errors->has('lname'))
                    <span class="text-danger">{{ $errors->first('lname') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label>Prénom:</label>
                <input type="text" name="fname" class="form-control" placeholder="Prenom">
                @if ($errors->has('fname'))
                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                @endif
            </div>
   
            <div class="form-group">
                <label>Date de naissance:</label>
                <input type="date" name="bdate" class="form-control" placeholder="Date de naissance">
                @if ($errors->has('bdate'))
                    <span class="text-danger">{{ $errors->first('bdate') }}</span>
                @endif
            </div>
    
            <div class="form-group">
                <label>Sexe:</label>
                <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="sexe" value="M"> M
                      <input type="radio" class="form-check-input" name="sexe" value="F"> F
                    </label>
                  </div>
                @if ($errors->has('sexe'))
                    <span class="text-danger">{{ $errors->first('sexe') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Téléphone:</label>
            <div class="input-group">
                <span class="input-group-addon">+212</span>
                <input type="text" class="form-control" name="phone" placeholder="Ex: 0601020304">
                
              </div>
              @if ($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Pays:</label>
                <select class="form-control" name="pays">
                     @foreach ($countries as $country)                        
                        <option value="{{$country->name}}">{{$country->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('country'))
                    <span class="text-danger">{{ $errors->first('country') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Date d'inscription:</label>
                <input type="date" name="rdate" class="form-control" placeholder="Date d'inscription">
                @if ($errors->has('rdate'))
                    <span class="text-danger">{{ $errors->first('rdate') }}</span>
                @endif
            </div>
            
            <div class="form-group">
                <label>Etat:</label>
                <span class="badge" style="background-color:<?php echo $etat ? 'green' : 'red' ; ?>">{{$etat ? 'Valide' : 'En attente' }}</span>
                <input type="hidden" name="etat" value="{{$etat ? 'Valide' : 'En attente' }}">
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-submit">ENVOYER</button>
            </div>
        </form>
    </div>
</body>

</html>